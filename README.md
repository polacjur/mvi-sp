# Popis problematiky
Táto semestrálna práca sa zaoberá problematikou predikcie toho, či užívateľ zostane po uplnutí skušobnej doby naďalej používať danú službu. Užívateľ po zaregistrovaní dostane 14 denný trial, v ktorom môže využívať služby naplno a zoznámiť sa s platformou Showmax. Cieľom tejto práce je detekovať na základe aktivít, ktoré uživateľ vykonal v priebehu prvých 7 dňoch, či "`churne"` alebo ostane.


# Predzpracovanie
PreprocessData.ipynb
PreprocessData_v2.ipynb

# Model
neural_network.ipynb

# Report
semestralka/report.pdf

# Popis vstupných dát pre neurónovú sieť
popis_dat.MD




# Prezentacia
https://docs.google.com/presentation/d/1YCb1NQql7CXPy9T1jEnOn_OT19oDnLdgSM8FCiUwpZU/edit?usp=sharing
https://docs.google.com/presentation/d/1YCb1NQql7CXPy9T1jEnOn_OT19oDnLdgSM8FCiUwpZU/edit?usp=sharing